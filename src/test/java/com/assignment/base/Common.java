package com.assignment.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Common functions which are useful for logger creation, loading properties and data generation.
 * <p/>
 * Created by shaikshavali on 20/1/17.
 */
public class Common {

    public static final String NUMBERS = "0123456789";

    /**
     * Creates logger and set properties.
     *
     * @param classname
     * @param filename
     * @param append
     * @return
     */
    public static final Logger createLogger(Class classname, String filename, boolean append) {
        SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");
        String date = date_format.format(new Date());
        filename = filename + "." + date;
        String path = Constants.userDir + Constants.separator + "logs";
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }

        Logger LOGGER = null;

        try {
            LOGGER = Logger.getLogger(classname.getName());
            LOGGER.setLevel(Level.ALL);
            FileHandler e = new FileHandler(path + Constants.separator + filename + ".txt", append);
            e.setFormatter(new SimpleFormatter());
            LOGGER.addHandler(e);
            LOGGER.setUseParentHandlers(false);
        } catch (SecurityException var9) {
            var9.printStackTrace();
        } catch (IOException var10) {
            var10.printStackTrace();
        }

        return LOGGER;
    }

    /**
     * Load properties to system.
     *
     * @param properties
     * @param file
     */
    public static void loadProperties(Properties properties, File file) {
        FileInputStream input = null;

        try {
            input = new FileInputStream(file.getAbsolutePath());
        } catch (FileNotFoundException var5) {
            var5.printStackTrace();
        }

        try {
            if (input != null) {
                properties.load(input);
            }
        } catch (IOException var4) {
            var4.printStackTrace();
        }

    }

    /**
     * Genrates random number with the given length.
     *
     * @param length
     * @return
     */
    public static Long generateRandomNumber(int length) {
        StringBuilder result = new StringBuilder();
        while (length > 0) {
            Random rand = new Random();
            result.append(NUMBERS.charAt(rand.nextInt(NUMBERS.length())));
            length--;
        }
        return Long.parseLong(result.toString());
    }
}
