package com.assignment.base;

/**
 * API Constants which are used in the API calls.
 * <p/>
 * Created by shaikshavali on 21/1/17.
 */
public class APIConstants {

    public static final String PROTOCOL_APP_JSON = "application/json";

    //Query params used for search number API
    public static final String COUNTRY_ISO = "country_iso";
    public static final String PATTERN = "pattern";
    public static final String TYPE = "type";
    public static final String LIMIT = "limit";

    public static final String NUMBERS = "numbers";
    public static final String NUMBER = "number";


    //Query params used for Message API
    public static final String SRC = "src";
    public static final String DST = "dst";
    public static final String TEXT = "text";

    public static final String MESSAGE_UUID = "message_uuid";

    public static final String OBJECTS = "objects";
    public static final String CASH_CREDITS = "cash_credits";
    public static final String MESSAGE = "message";
    public static final String TOTAL_RATE = "total_rate";
    public static final String OUTBOUND = "outbound";
    public static final String RATE = "rate";
}
