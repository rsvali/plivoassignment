package com.assignment.base;

/**
 * Constants which are used with project properties.
 * <p/>
 * Created by shaikshavali on 20/1/17.
 */
public class Constants {

    public static final String separator = System.getProperty("file.separator");
    public static final String userDir = System.getProperty("user.dir");
    public static final String propertiesDir;

    //map the property name from plivotest.properties
    public static final String endpoint = "endpoint";
    public static final String authId = "authId";
    public static final String authToken = "authToken";


    static {
        propertiesDir = userDir + separator + "src" + separator + "test" + separator + "resources" + separator + "com" + separator + "assignment" + separator + "properties" + separator;
    }
}
