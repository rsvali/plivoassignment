package com.assignment.base;

import com.assignment.utils.RestUtil;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.io.File;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * This is base class for the Test, responsible for loading of the environment properties.
 * <p/>
 * Created by shaikshavali on 19/1/17.
 */
public class TestBase {

    public static final Logger LOG = Common.createLogger(TestBase.class, "logs", true);

    public Properties envproperties = new Properties();
    protected RestUtil restUtil;

    @Parameters({"env"})
    @BeforeSuite
    protected void setup(@Optional("plivotest") String environment) {
        Common.loadProperties(this.envproperties, new File(Constants.propertiesDir + environment + ".properties"));
        String baseUrl = envproperties.getProperty(Constants.endpoint);
        System.setProperty(Constants.authId, envproperties.getProperty(Constants.authId));
        System.setProperty(Constants.authToken, envproperties.getProperty(Constants.authToken));
        restUtil = new RestUtil(baseUrl, APIConstants.PROTOCOL_APP_JSON, APIConstants.PROTOCOL_APP_JSON);
    }

}
