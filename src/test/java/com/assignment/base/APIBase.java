package com.assignment.base;

import com.jayway.restassured.response.Response;
import org.testng.Assert;

import java.util.Map;

/**
 * This is the base class for the API. It contains all end point for the API's and basic http status validation.
 * <p/>
 * Created by shaikshavali on 21/1/17.
 */
public class APIBase extends TestBase {

    private static String ACCOUNT = "v1/Account/authId/";
    private static String SEARCH_NUMBER = "v1/Account/authId/PhoneNumber";
    private static String BUY_NUMBER = "v1/Account/authId/PhoneNumber/number/";
    private static String MESSAGE = "v1/Account/authId/Message";
    private static String GET_MESSAGE = "v1/Account/authId/Message/message_uuid/";
    private static String PRICING = "v1/Account/authId/Pricing/";

    /**
     * Replaces authId with the user authId
     *
     * @param apiEndpoint
     * @return api endpoint updated with authId
     */
    private String replaceAuthId(String apiEndpoint) {
        return apiEndpoint.replace(Constants.authId, System.getProperty(Constants.authId));
    }

    /**
     * Validate Response with 200 status code
     *
     * @param response Response
     */
    protected void validateResp(Response response) {
        validateResp(response, 200);
    }

    /**
     * Validate Response with expCode status code
     *
     * @param response Response
     * @param expCode  expected status code
     */
    protected void validateResp(Response response, int expCode) {
        Assert.assertEquals(response.getStatusCode(), expCode);
    }

    /**
     * Get Account details and validate successful API call.
     *
     * @return Response from API
     */
    public Response getAccount() {
        String apiEndpoint = replaceAuthId(ACCOUNT);
        Response resp = restUtil.callGet(apiEndpoint);
        validateResp(resp);
        return resp;
    }

    /**
     * Search number with given query params and validate successful API call.
     *
     * @param params query params for the API
     * @return Response from API
     */
    public Response searchNumber(Map<String, Object> params) {
        String apiEndpoint = replaceAuthId(SEARCH_NUMBER);
        Response resp = restUtil.callGet(apiEndpoint, params);
        validateResp(resp);
        return resp;
    }

    /**
     * Buy given number validate successful API call.
     *
     * @param number Number to buy
     * @return Response from API
     */
    public Response buyNumber(String number) {
        String apiEndpoint = replaceAuthId(BUY_NUMBER).replace(APIConstants.NUMBER, number);
        Response resp = restUtil.callPost(apiEndpoint);
        validateResp(resp, 201);
        return resp;
    }

    /**
     * Message between numbers with given query params and validate successful API call.
     *
     * @param params query params for the API
     * @return Response from API
     */
    public Response message(Map<String, Object> params) {
        String apiEndpoint = replaceAuthId(MESSAGE);
        Response resp = restUtil.callPost(apiEndpoint, params);
        validateResp(resp, 202);
        return resp;
    }

    /**
     * Get Message details with messageUUID and validate successful API call.
     *
     * @param messageUUID message unique ID.
     * @return Response from API
     */
    public Response getMessage(String messageUUID) {
        String apiEndpoint = replaceAuthId(GET_MESSAGE).replace(APIConstants.MESSAGE_UUID, messageUUID);
        Response resp = restUtil.callGet(apiEndpoint);
        validateResp(resp);
        return resp;
    }

    /**
     * Get pricing details for the country and validate  successful API call.
     *
     * @param params query parrams for the API
     * @return Response from API
     */
    public Response pricing(Map<String, Object> params) {
        String apiEndpoint = replaceAuthId(PRICING);
        Response resp = restUtil.callGet(apiEndpoint, params);
        validateResp(resp);
        return resp;
    }
}
