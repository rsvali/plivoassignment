package com.assignment.utils;

import com.assignment.base.Common;
import com.assignment.base.Constants;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

import java.util.Map;
import java.util.logging.Logger;

/**
 * This class is responsible to RestAssurred object handle, calling API with different parameters.
 * <p/>
 * Created by shaikshavali on 20/1/17.
 */
public class RestUtil {

    private static final Logger LOG = Common.createLogger(RestUtil.class, "logs", true);
    private String baseUrl;
    private String contentType;
    private String accept;

    public RestUtil() {
        RestAssured.reset();
        RestAssured.useRelaxedHTTPSValidation();
    }

    public RestUtil(String baseUrl, String contentType, String accept) {
        RestAssured.reset();
        RestAssured.useRelaxedHTTPSValidation();
        this.baseUrl = baseUrl;
        this.contentType = (contentType != null) ? contentType : "*/*";
        this.accept = (accept != null) ? accept : "*/*";
    }

    /**
     * Sets basic auth details to RestAssured
     * <TODO>Need to handle different auth details in same suite</TODO>
     *
     * @return RequestSpecification
     */
    public RequestSpecification setAuthorization() {
        return RestAssured.given().auth().basic(System.getProperty(Constants.authId), System.getProperty(Constants.authToken));
    }

    /**
     * Calls Get API for the given path.
     *
     * @param path API endpoint
     * @return Response from the API
     */
    public Response callGet(String path) {
        Response response = setAuthorization()
                .contentType(contentType)
                .log().all()
                .when().get(baseUrl + path);
        response.then().log().all();
        return response;
    }

    /**
     * Call Get API with the query params.
     *
     * @param path   API endpoint
     * @param params Query params to set for the API
     * @return Response from the API
     */
    public Response callGet(String path, Map<String, Object> params) {
        Response response = setAuthorization()
                .contentType(contentType)
                .queryParams(params)
                .log().all()
                .when().get(baseUrl + path);
        response.then().log().all();
        return response;
    }

    /**
     * Calls Post API for the given path.
     *
     * @param path API endpoint
     * @return Response from the API
     */
    public Response callPost(String path) {
        Response response = setAuthorization()
                .contentType(contentType)
                .log().all()
                .when().post(baseUrl + path);
        response.then().log().all();
        return response;
    }

    /**
     * Call Post API with the query params.
     *
     * @param path   API endpoint
     * @param params Query params to set for the API
     * @return Response from the API
     */
    public Response callPost(String path, Map<String, Object> params) {
        Response response = setAuthorization()
                .contentType(contentType)
                .queryParams(params)
                .log().all()
                .when().post(baseUrl + path);
        response.then().log().all();
        return response;
    }

}
