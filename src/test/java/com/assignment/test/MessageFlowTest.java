package com.assignment.test;

import com.assignment.base.APIBase;
import com.assignment.base.APIConstants;
import com.jayway.restassured.response.Response;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a test class which includes different flows to test message.
 * <p/>
 * Created by shaikshavali on 21/1/17.
 */
public class MessageFlowTest extends APIBase {

    @Test
    public void testMessageFlow() {
        //Search 2 numbers with the pattern.
        Map<String, Object> searchNumberParams = prepareSearchNumberParams();
        Response searchResp = searchNumber(searchNumberParams);
        List<Map<String, String>> numbers = searchResp.jsonPath().get(APIConstants.OBJECTS);

        //check at least 2 numbers exist for the given pattern.
        if (null == numbers || numbers.size() < 2)
            Assert.fail("No results/required no. results found for the given pattern ");

        String number1 = numbers.get(0).get(APIConstants.NUMBER);
        String number2 = numbers.get(1).get(APIConstants.NUMBER);

        //buy first number
        Response buyNumbResp1 = buyNumber(number1);
        validateBuyNumberResponse(number1, buyNumbResp1);

        //buy second number
        Response buyNumbResp2 = buyNumber(number2);
        validateBuyNumberResponse(number2, buyNumbResp2);

        //get account details and get cash credit in the account.
        Response accRespMsgBfr = getAccount();
        double cashCreditBfrMsg = accRespMsgBfr.jsonPath().get(APIConstants.CASH_CREDITS);

        //Message from number1 to number2 and get message UUID
        Response messageResp = message(prepareMessageParams(number1, number2, APIConstants.MESSAGE));
        String messageUUID = messageResp.jsonPath().get(APIConstants.MESSAGE_UUID);
        Assert.assertNotNull(messageUUID);

        //get message detail(message rate) using messageUUID
        Response getMessageResp = getMessage(messageUUID);
        Double msgRate = getMessageRate(getMessageResp, messageUUID);
        Assert.assertNotNull(msgRate, "Message rate is not found for message with uuid: " + messageUUID);

        //get pricing details(outbound rate) for the country
        Response pricingResp = pricing(preparePricingParams(searchNumberParams.get(APIConstants.COUNTRY_ISO).toString()));
        Double outboundRate = getOutboundRate(pricingResp);
        Assert.assertNotNull(outboundRate, "Outbound rate is null for Country: " + searchNumberParams.get(APIConstants.COUNTRY_ISO));

        //verify total rate from message detail response and outbound rate from pricing response
        Assert.assertEquals(msgRate, outboundRate, "Message Rate and Pricing -> Outbound rate are not matching");

        //get account details and get cash credit after message.
        Response accRespMsgAft = getAccount();
        double cashCreditAfrMsg = accRespMsgAft.jsonPath().get(APIConstants.CASH_CREDITS);

        //validate the amount deducted is same as rate mentioned
        Assert.assertEquals(cashCreditBfrMsg - msgRate, cashCreditAfrMsg, "credit amount is not matching with the expected");
    }

    /**
     * Get Message rate from the response.
     *
     * @param getMessageResp GetMessage API response.
     * @param messageUUID    Message UUID - unique ID for the messaage
     * @return messageRate
     */
    public Double getMessageRate(Response getMessageResp, String messageUUID) {
        List<Map<String, String>> messages = getMessageResp.jsonPath().get(APIConstants.OBJECTS);
        for (Map<String, String> message : messages) {
            if (message.get(APIConstants.MESSAGE_UUID).equals(messageUUID))
                return Double.parseDouble(message.get(APIConstants.TOTAL_RATE));
        }
        return null;
    }

    /**
     * Get Outbound rate from Pricing API response.
     *
     * @param pricingResp Pricing API response.
     * @return outbound rate
     */
    public Double getOutboundRate(Response pricingResp) {
        JSONObject messageObj = pricingResp.jsonPath().get(APIConstants.MESSAGE);
        JSONObject outboundObj = (JSONObject) messageObj.get(APIConstants.OUTBOUND);
        return Double.parseDouble(outboundObj.get(APIConstants.RATE).toString());
    }

    /**
     * Prepares query params map for search number API.
     *
     * @return Map of query params.
     */
    public Map<String, Object> prepareSearchNumberParams() {
        Map<String, Object> data = new HashMap<>();
        data.put(APIConstants.COUNTRY_ISO, "US");
        //random pattern also can be generated
        //Long pattern = Common.generateRandomNumber(6);
        data.put(APIConstants.PATTERN, 2239);
        data.put(APIConstants.LIMIT, 2);
        return data;
    }

    /**
     * Prepares query params map for Message API.
     *
     * @param source source number
     * @param dest   destination number
     * @param text   message to send
     * @return Map of query params.
     */
    public Map<String, Object> prepareMessageParams(String source, String dest, String text) {
        Map<String, Object> params = new HashMap<>();
        params.put(APIConstants.SRC, source);
        params.put(APIConstants.DST, dest);
        params.put(APIConstants.TEXT, text);
        return params;
    }

    /**
     * Prepares query params map for Pricing API.
     *
     * @param countryIso country
     * @return Map of query params.
     */
    public Map<String, Object> preparePricingParams(String countryIso) {
        Map<String, Object> params = new HashMap<>();
        params.put(APIConstants.COUNTRY_ISO, countryIso);
        return params;
    }

    /**
     * Validate Buy number API response. Validates buy number is successful.
     *
     * @param number   Number bought
     * @param response Buy Number API response
     */
    public void validateBuyNumberResponse(String number, Response response) {
        List<Map<String, String>> numbers = response.jsonPath().get(APIConstants.NUMBERS);
        Assert.assertEquals(numbers.get(0).get(APIConstants.NUMBER), number, "Buy Number validation");
        Assert.assertEquals(numbers.get(0).get("status"), "Success", "Buy Number status validation");
    }

}
